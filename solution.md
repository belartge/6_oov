Сама лаба состоит в обработке текстового файла. По заданию, имя файла должно совпадать с фамилией сдающего лабу. В этом репозитории я привел пример своего текстового файла. Ты можешь или сгенерировать новый, или взять за основу мой (но тогда **не забудь** переименовать файлик, не пались, ну).

После того, как ты установишь Spark на компьютер, открой командную строку, в ней открой папку с установленным спарком. Вызываешь команду `spark-shell` и ждешь, пока jvm не закончит писать свои строки в консоль. 

Поочередно вставь предложенные ниже строки

1) Объявление источника данных, открытие файла. Тут лучше указать полный путь до файла
```
val text = sc.textFile("/home/legion/yapparov")
```
2) Подключи поддержку RDD
```
import org.apache.spark.rdd.RDD
```
3) Начни решать задачи

* найти заданное слово
```
  def deletePunctuation(rdd: RDD[String]): RDD[String] = {
    rdd.flatMap(_.split("\\s")).map(_.replaceAll("""["'-,.!?:;]""", ""))
  }

  def searchWord(rdd: RDD[String], word: String): Unit = {
    val linesWithIndex = rdd.flatMap(_.split("\\n")).zipWithIndex().filter({ case (line: String, index: Long) => line.contains(word)})
    linesWithIndex.foreach({
      case (line: String, indexOfRow: Long) => {
        val indexInRow = line.indexOf(word)
        println(indexOfRow, indexInRow)
      }
    })
  }
```
* посчитать количество вхождений заданного слова
```
  def searchAndCount(rdd: RDD[String], word: String): Int = {
    val result = rdd.flatMap(_.split("\\s")).map(_.replaceAll("""["'-,.!?:;]""", "").trim.toLowerCase).filter(!_.isEmpty).filter({ case x: String => x == word }).map((_, 1)).reduceByKey(_ + _).sortByKey().collect()
    if (result.length == 0) {
      return 0
    }
    result(0)._2
  }
```
* разбить текст на слова и удалить пустые строки
```
  def transform(rdd: RDD[String]): RDD[(String, Int)] = {
    rdd.flatMap(_.split("\\s")).map(_.replaceAll("""["'-,.!?:;]""", "").trim.toLowerCase).filter(!_.isEmpty).map((_, 1)).reduceByKey(_ + _).sortByKey()
  }
```

Одной из задач задания было также "Создать   наборы   RDD   на   основе   массивов   (целочисленных   и ассоциативных) и применить к ним Transformation reduce и map", но это и так было сделано в описанных выше пунктах (в частности, в последнем коде)

После того, как вставишь код выше, настанет время запустить эти функции
1. `searchWord(text, "yes")`.  Если запустить это на моем файле, то получится следующее
```
(0,1519)
(8,366)
(14,886)
(20,1397)
(28,49)
(34,366)
(40,444)
(46,444)
(52,444)
(58,444)

```
То есть, получили картеж, включающий номер строки и номер слова в строке.
2. `searchAndCount(text, "yes")`.  Выдаст `Int = 10`
3. `transform(text)`.  Создаст массив наборов RDD. Чтобы посмотреть их содержимое, введи `transform(text).collect()`

---

Если будет что не получаться или возникнут вопросы - пиши, адрес преждний :)
